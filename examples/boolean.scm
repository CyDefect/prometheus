(add-to-load-path (dirname (getcwd)))
(define-module (prometheus boolean)
  #:use-module (prometheus)
  #:use-module (prometheus x)
  #:use-module (srfi srfi-64)
  #:export (*boolean*
            *true*
            *false*))

(define *boolean*
  (let ([o (*object* 'clone)])
    (o 'clone-method-responsibilities:
       '(not ifTrue: ifFalse: asSchemeValue))
    (o 'name: "*boolean*")
    o))

(define *true* #f)
(define *false* #f)

(set! *true*
  (let ([o (*boolean* 'clone)])

    (o 'implement! 'not
       (lambda (self resend)
         *false*))

    (o 'implement! 'asSchemeValue
       (lambda (self resend)
         #t))

    (o 'implement! 'ifTrue:
       (lambda (self . args)
         ;;(self . resender trueBlock ifFalse: falseBlock)
         (apply (if (member 'ifFalse: args)
                    (lambda (self . args)
                      ((list-ref args 1)))
                    (lambda (self . args)
                      ((list-ref args 1))))
                (cons self args))))

    (o 'implement! 'ifFalse:
       (lambda (self . args)
         ;;(self . resender falseBlock ifTrue: trueBlock)
         (apply (if (member 'ifTrue: args)
                    (lambda (self . args)
                      ((list-ref args 3)))
                    (lambda (self . args) ;; Nil.
                      (list)))
                (cons self args))))

    (o 'name: "*true*")
    o))

(set! *false*
  (let ([o (*boolean* 'clone)])

    (o 'implement! 'not
       (lambda (self resend)
         *true*))

    (o 'implement! 'asSchemeValue
       (lambda (self resend)
         #f))

    (o 'implement! 'ifTrue:
       (lambda (self . args)
         ;;(self . resender trueBlock ifFalse: falseBlock)
         (apply (if (member 'ifFalse: args)
                    (lambda (self . args)
                      ((list-ref args 3)))
                    (lambda (self . args) ;; Nil.
                      (list)))
                (cons self args))))

    (o 'implement! 'ifFalse:
       (lambda (self . args)
         (apply (if (member 'ifTrue: args)
                    (lambda (self . args)
                      ((list-ref args 1)))
                    (lambda (self . args)
                      ((list-ref args 1))))
                (cons self args))))

    (o 'name: "*false*")
    o))
(test-begin "prometheus-boolean-test")
(test-assert
    ;;=> # t
    (eq? *true* (*false* 'not)))

(test-assert
    ;;=> 4
    (= 4
       (*true*  'ifTrue: (lambda () 4) 'ifFalse: (lambda () 2))))

(test-assert
    ;;=> 2
    (= 2
       (*false* 'ifTrue: (lambda () 4) 'ifFalse: (lambda () 2))))

(test-assert
    ;;=> '(), i.e. Nil
    (eq? '()
          (*true* 'ifFalse: (lambda () 2))))

(test-assert
    (eq? '()
         (*true* 'implementor-messages)))

(test-assert
    (eq? '()
         (*false* 'implementor-messages)))
(test-assert
    (equal? '(asSchemeValue ifFalse: ifTrue: not)
            (*boolean* 'implementor-messages)))

(test-end "prometheus-boolean-test")
