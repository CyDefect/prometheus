(add-to-load-path (dirname (getcwd)))
(use-modules (prometheus) (hermes) (ice-9 pretty-print))
(let ((o (make-prometheus-root-object)))

  (o 'add-value-slot! 'name 'name: "*o*")
  ;; wrap `self name:
  (let ((base-setter
         ;; we cannot reference the `self name' reference in this procedure
         ;; therefore we steal it from the message list
         (cdr (assoc 'name: (messages-alist (o 'messages))))))
    (o 'add-method-slot! 'name:
       (lambda (self resend new)
         (base-setter self resend new)
         (set-procedure-property! self 'name (string->symbol new)))))
  (o 'name: "*o*")

  ;; add responsibility error methods
  (o 'add-method-slot! 'clone-responsibility
    (lambda (self resend message)
      (error "Message is clone's responsibility to implement" self message)))

  (o 'add-method-slot! 'clone-method-responsibility
    ;; define a method at the message and define it for the clone
    (lambda (self resend message)
      (self 'add-method-slot! message
            (lambda (self . _)
              (self 'clone-responsibility message)))))

  (o 'add-method-slot! 'clone-method-responsibilities:
    (lambda (self resend messages)
      (map (lambda (m)
             (self 'add-method-slot! m
                   (lambda (self . _) (self 'clone-responsibility m))))
           messages)))

  (pretty-print
   (list
    (let ((a #f))

      (o 'clone-method-responsibility 'at:)
      (set! a (o 'clone))

      (a 'add-value-slot! 'arr 'arr: #())

      (a 'add-method-slot! 'at:
         (lambda (self resender index)
           (vector-ref (self 'arr) index)))

      (a 'arr: #(1 2 3))
      ;; return
      (list
       (cons ':a-arr (a 'arr))
       (cons ':a-at-1 (a 'at: 1))))

    (let ((a #f))

      (o 'clone-method-responsibilities: '(at: collect:))
      (set! a (o 'clone))

      (a 'add-value-slot! 'arr 'arr: #())

      (a 'add-method-slot! 'at:
         (lambda (self resender index)
           (vector-ref (self 'arr) index)))

      (a 'add-method-slot! 'collect:
         (lambda (self resender block)
           (let ((o (self 'clone)))
             (o 'arr:
                (list->vector
                 (map block
                      (vector->list (self 'arr)))))
             o)))

      (a 'arr: #(1 2 3))
      (a 'name: "*a*")

      (let ((collection (a 'collect: 1+)))
        ;; return
        (list (cons ':a a)
              (cons ':a-at-1 (a 'at: 1))
              (cons ':collection-ar (collection 'arr))
              (cons ':collection-at-1 (collection 'at: 1))))))))
