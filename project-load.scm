(add-to-load-path (getcwd))
(map load '("hermes.scm" "prometheus.scm" "prometheus-x.scm"))
(use-modules (hermes) (prometheus) (prometheus x))
