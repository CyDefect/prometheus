(add-to-load-path (getcwd))
(define-module (prometheus x)
  #:use-module (prometheus)
  #:use-module (srfi srfi-1)
  #:use-module (hermes)
  #:export (cascade
            comment
            wrap-method
            *slot-setters*))

;;; VALUE SLOT WRAPPER
(define *slot-setters*
  (let ([o (*object* 'clone)])

    (o 'add-value-slot! 'default 'default: 'bang)
    (o 'add-value-slot! 'selectors 'selectors:
       '((kwarg . "~a:")
         (bang  . "set-~a!")))

    (let* ((method 'selectors:)
           (selectors:-proc (cdr (assoc method (messages-alist (o 'messages))))))

      (o 'delete-message! method)
      (o 'add-method-slot! 'add-selector:
         (lambda (self resend elt)
           (let ((ret (cons elt (self 'selectors))))
             (selectors:-proc self resend ret)
             ret)))

      (o 'add-method-slot! 'remove-selector:
         (lambda (self resend elt-or-pred)
           (let ((ret
                  (remove (if (procedure? elt-or-pred)
                              elt-or-pred
                              (lambda (x) (equal? (car x) elt-or-pred)))
                          (self 'selectors))))
             (selectors:-proc self resend ret)
             ;; choose first in list as default if its not present.
             (unless (assoc (self 'default) ret)
               (self 'default: (car (self 'styles))))
             ret))))

    (o 'add-method-slot! 'styles
       (lambda (self resend)
         (map car (self 'selectors))))

    (o 'add-method-slot! 'member:
       (lambda (self resend elt)
         (let ((m (member elt (self 'styles))))
           (if m (car m) m))))

    (o 'add-method-slot! 'selector-fmt:
       (lambda (self resend elt)
         (cdr (assoc (if (self 'member: elt)
                         elt
                         (self 'default))
                     (self 'selectors)))))

    o))

(*object* 'add-method-slot! 'add-value-slots:
  (lambda (self . args)
    ;; (self . args) as i do not want to figure out optional arguments
    ;; (self slot-resender slot-alist style-resend getter-style)
    (let* ([slot-alist (cadr args)]
           [style-resend (member 'style: args)]
           [getter-style (when style-resend
                           (cadr style-resend))])
      (map (lambda (s)
             (let* ([slot-name (car s)]
                    [style-fmt
                    ;;getter-style if #<void> will return *slot-setters*.default
                     (*slot-setters* 'selector-fmt: getter-style)]
                    [slot-setter
                     (string->symbol
                      (format #f style-fmt
                              (symbol->string slot-name)))])
               (apply self
                      `(add-value-slot! ,slot-name
                        ,@(if (eq? 'mut (list-ref s 1))
                              (list slot-setter (list-ref s 2))
                              (cdr s))))
               (cons slot-name slot-setter)))
           slot-alist))))

;;; IMPLEMENTOR MESSAGE INTERFACE
(*object* 'add-value-slot! 'implementor-messages 'implementor-messages: '())

(*object* 'add-method-slot! 'add-implementor-message:
  (lambda (self resender message)
    (self 'implementor-messages:
          (cons message
                (self 'implementor-messages)))))

(*object* 'add-method-slot! 'implement!
  (lambda (self resender message block)
    (self 'implementor-messages: (remove (lambda (x) (eq? message x))
                                         (self 'implementor-messages)))
    (self 'add-method-slot! message block)))


;;; CLONE METHOD RESPONSIBILITIES
(*object* 'add-method-slot! 'clone-responsibility
  (lambda (self resend message)
    (error "Message is clone's responsibility to implement" self message)))

(*object* 'add-method-slot! 'clone-method-responsibility
  (lambda (self resend message)
    (self 'add-implementor-message: message)
    (self 'add-method-slot! message
          (lambda (self . _) (self 'clone-responsibility message)))
    message))

(*object* 'add-method-slot! 'clone-method-responsibilities:
  (lambda (self resend messages)
    (map (lambda (m) (self 'clone-method-responsibility m) m)
         messages)))

;;; NAME PRINTER
(*object* 'add-value-slot! 'name 'name: "*object*")
;; wrap original name:
(let* ((method 'name:)
       (base-setter
        ;; we cannot reference the `self name' reference in this procedure
        ;; therefore we steal it from the message list
        (cdr (assoc method (messages-alist (*object* 'messages))))))
  (*object* 'delete-message! method)
  (*object* 'add-method-slot! method
     (lambda (self resend new)
       (base-setter self resend new)
       (set-procedure-property! self 'name (string->symbol new))
       ;; workaround because apparently calls copy the Original method to
       ;; themselves without the copy-method! expr in the older impl of
       ;; make-getter-setter
       ;(unless (eq? *object* self) (self 'delete-message! method))
       new)))

(*object* 'name: (*object* 'name))

(define-syntax wrap-method
  (syntax-rules ()
   ((_ (object method) baseproc)
    (let ((methodproc
           (cdr
            (assoc method
                   (messages-alist
                    (object 'messages))))))
      (object 'delete-message! method)
      (object 'add-method-slot! method
              (apply baseproc
                     (list methodproc)))))))

#||
(wrap-method (*object* 'name:)
  (lambda (baseproc)
    (lambda (self resend new)
      (baseproc self resend new)
      (set-procedure-property! self 'name (string->symbol new)))))
||#

;;; Cascading
(*object* 'add-method-slot! 'yourself (lambda (self resend) self))

(define (cascade object messages)
  (let lp ((o object)
           (m messages)
           (ret '()))
    (cond
     [(null? m) o]
     [else #;(format #t "---~%o:=~a~%m.car:=~a~%" o (car m))
      (let* ((msg
              (if (and #t
                       ;;;; this should do arity checking ideally.
                       ;; (1- (car (procedure-minimum-arity (cdr (assoc (car m) (o 'messages))))))
                       ;;;; this is non-standard and relies on the guile-specific
                       ;;;; procedure `procedure-minimum-arity'
                       (not (list? (car m))))
                  (list (car m))
                  ;;^~~~~~~~~~~~~^
                  ;; only supposed to be unary messages
                  (car m)))
             (ret (apply o msg)))
        ;; make sure the return value is an object for all except the last value
        (lp (if (or (procedure? ret) (= (length m) 1))
                ret
                o)
            (cdr m)
            ret))])))

(define-syntax comment (syntax-rules () [[_ ...] '()]))
(comment
 (cascade *object*
          `(clone
            (add-method-slot! go-fuck ,(lambda (self resender) 'yourself))
            go-fuck))
 ===> 'yourself)

